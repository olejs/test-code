<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Price;
use App\Models\Product;
use App\Models\User;
use Illuminate\Console\Command;

class ImportPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import prices from csv file';

    protected $filePath = '/home/olejs/Projects/test-code/public/import.csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(($handle = fopen($this->filePath, 'r')) !== false)
        {
//              $start = microtime(true);
            // get the first row, which contains the column-titles
            $header = fgetcsv($handle);

            // loop through the file line-by-line
            while(($data = fgetcsv($handle)) !== false)
            {
                //sku 0
                //account_ref 1
                //user ref 2
                $product = Product::where('sku', $data[0])->first();
                $account = Account::where('external_reference', $data[1])->first();
                $user    = User::where('external_reference', $data[2])->first();

                Price::insert([
                    'product_id' => $product->id,
                    'account_id' => empty($account) ? null : $account->id,
                    'user_id'    => empty($user) ? null : $user->id,
                    'quantity'   => $data[3],
                    'value'      => $data[4]
                ]);

                unset($account);
                unset($user);
                unset($product);
                unset($data);
            }

//            $time_elapsed_secs = microtime(true) - $start;
//            dd($time_elapsed_secs);

            fclose($handle);
        }
    }
}
