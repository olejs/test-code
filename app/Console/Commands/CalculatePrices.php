<?php

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class CalculatePrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:calculate {skus*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate the lowest price';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $account_id = $this->ask('What is your account id?');
        $redis = Redis::connection();

        if(empty($account_id)) {

            $products = Product::with('publicPrices')->withMin('publicPrices', 'value')->whereIn('sku', $this->argument('skus'))->get();

            $i = 0;
            foreach ($products as $p) {

                $this->info('[DB] Public lowest price '.$p->public_prices_min_value);
                $feed = $redis->get($this->argument('skus')[$i]);
                if(!empty($feed)) {
                    $this->info('[Live feed] public price '.$feed);
                }
                $i++;
            }

        } else {
            $products = new Product();
            $products = $products->with('accountPrices')->withMin('accountPrices', 'value')->whereIn('sku', $this->argument('skus'))->get();

            foreach ($products as $p) {

                //to do.....
            }
        }
    }
}
