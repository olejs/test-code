<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

class FetchPricesIntoRedis extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch prices from live_prices.json into redis cache';

    protected $filePath = '/home/olejs/Projects/test-code/public/live_prices.json';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $json = json_decode(file_get_contents($this->filePath), true);
//        $start = microtime(true);
        $count = count($json);
        $i = 1;
        $redis = Redis::connection();
        foreach($json as $element) {
            if(isset($element['account'])) {
                  $redis->set($element['sku'].'_'.$element['account'], $element['price']);
            } else {
                $redis->set($element['sku'], $element['price']);
            }
            $i++;

            if($i%100 == 0) {
                echo "Processed: ".$i."/".$count.PHP_EOL;;
            }
        }
//        $time_elapsed_secs = microtime(true) - $start;
//        dd($time_elapsed_secs);
    }
}
