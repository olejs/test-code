<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $account_id;
    //ralations
    public function publicPrices() {
        return $this->hasMany(Price::class, 'product_id', 'id')->whereNull('account_id')->whereNull('user_id');
    }

    public function accountPrices() {
        return $this->hasMany(Price::class, 'product_id', 'id');
    }
}
